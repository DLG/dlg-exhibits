# frozen_string_literal: true

##
# Global Spotlight helpers
module SpotlightHelper
  include ::BlacklightHelper
  include Spotlight::MainAppHelpers
  def solr_document_description(document)
    if document&.response.dig("response", "docs")&.first.dig("spotlight_upload_description_tesim").blank?
      ""
    else
      document.response.dig("response", "docs")&.first.dig("spotlight_upload_description_tesim")
    end
  end
end
