# app/helpers/image_protection_helper.rb
module ImageProtectionHelper
  def conditionally_restrict(solr_doc, exhibit)
    if defined?(solr_doc)
      hash_key = 'exhibit_' + exhibit.slug + '_restriction_tesim'
      if solr_doc._source.key?(hash_key)
        if solr_doc._source[hash_key].include?('AJC')
          "oncontextmenu='return false;'".html_safe
        end
      end
    end
  end
end
