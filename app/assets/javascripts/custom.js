/* disable right-click on images */
window.addEventListener('load', function () {
    $('[oncontextmenu="return false;"]').find('.openseadragon-canvas').attr('oncontextmenu', 'return false;');

    /* ITEM RECORDS TWEAKS */
    if ($(".dl-horizontal")[0]) {
        /* hide empty item records fields */
        $('.dl-horizontal dd:empty').each(function () {
            var myClass = this.className;
            $('.' + myClass).remove();
        });

        /* make links in item records clickable */
        $('dd').each(function(){
            // Get the content
            var str = $(this).html();
            // Set the regex string
            var regex = /(https?:\/\/([-\w\.]+)+(:\d+)?(\/([\w\/_\.\-\:\%\,\#]*(\?\S+)?)?)?)/ig
            // Replace plain text links by hyperlinks
            var replaced_text = str.replace(regex, "<a href='$1' target='_blank'>$1</a>");
            // Echo link
            $(this).html(replaced_text);
        });
    }
})
