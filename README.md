# DLG Spotlight

DLG Exhibits will be a partnership between the Digital Library of Georgia and 
the New Georgia Encyclopedia to bring curated exhibits using DLG materials to
the public.

## Installation

Prereqs: Ruby `2.5.5`, Vagrant, `imagemagick` (install via `apt`). `master.key` 
file from mak@uga.edu.

1. Clone repo
2. Build virtual environment for backend services with `vagrant up`. This will take a while as it sets up 
VMs for the Postgres DB and Solr.
3. Install and configure application gems with `bundle install`
4. Initialize DB: `bundle exec rake db:setup`
5. Run dev server: `bundle exec rails s`

## Development

### CSS and JS Assets
Site CSS is contained in `/app/assets/stylesheets`. JavaScript in 
`/app/assets/javascripts`. Blacklight and Spotlight styles are in there too but 
can be overridden in `/app/assets/stylesheets/application.css` and 
`/app/assets/javascript/application.js`. 

### Testing

TODO