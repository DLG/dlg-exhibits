# frozen_string_literal: true

source 'https://rubygems.org'
git_source(:github) { |repo| "https://github.com/#{repo}.git" }

ruby '2.6.6'

gem 'blacklight', ' ~> 6.0'
gem 'blacklight-gallery', '>= 0.3.0'
gem 'blacklight-oembed', '~> 0.3'
gem 'blacklight-spotlight', github: 'projectblacklight/spotlight', tag: 'v2.13.0'
gem 'bootsnap', '>= 1.1.0', require: false
gem 'bootstrap-sass', '~> 3.0'
gem 'carrierwave'
gem 'coffee-rails', '~> 4.2'
gem 'devise'
gem 'devise-guests', '~> 0.6'
gem 'devise_invitable'
gem 'exception_notification'
gem 'font-awesome-rails'
gem 'friendly_id'
gem 'jbuilder', '~> 2.5'
gem 'jquery-rails'
gem 'nokogiri', '>= 1.11.0.rc4'
gem 'pg'
gem 'puma'
gem 'rails', '5.2.8.1'
gem 'redcarpet', '>= 3.5.1'
gem 'rsolr', '>= 1.0', '< 3'
gem 'sass-rails', '~> 5.0'
gem 'sitemap_generator'
gem 'slack-notifier'
gem 'turbolinks', '~> 5'
gem 'twitter-typeahead-rails', '0.11.1.pre.corejavascript'
gem 'tzinfo-data', platforms: [:mingw, :mswin, :x64_mingw, :jruby]
gem 'uglifier', '>= 1.3.0'

group :development do
  gem 'letter_opener'
  gem 'listen', '>= 3.0.5', '< 3.2'
  gem 'rubocop', require: false
  gem 'spring'
  gem 'spring-watcher-listen', '~> 2.0.0'
  gem 'web-console', '>= 3.3.0'
  gem 'xray-rails'
end

group :test do
  gem 'capybara', '>= 2.15'
  gem 'chromedriver-helper'
  gem 'selenium-webdriver'
end

group :development, :test do
  gem 'byebug', platforms: [:mri, :mingw, :x64_mingw]
end
