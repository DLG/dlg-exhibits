#!/bin/sh -e

# install solr
wget https://ai.galib.uga.edu/files/solr-6.5.1.tgz
tar -xvf solr-6.5.1.tgz

# copy solr config from Project code
mkdir solr-6.5.1/server/solr/configsets/dlg-spotlight
mkdir solr-6.5.1/server/solr/configsets/dlg-spotlight/conf
cp -r /vagrant/solr/conf solr-6.5.1/server/solr/configsets/dlg-spotlight/

# start solr, creating collections
solr-6.5.1/bin/solr start -c
solr-6.5.1/bin/solr create -c dlg-spotlight-dev -d dlg-spotlight
solr-6.5.1/bin/solr create -c dlg-spotlight-test -d dlg-spotlight

