# frozen_string_literal: true

Rails.application.config.to_prepare do
  Spotlight::PagesController.module_eval do
    def page_collection_name
      controller_name.to_sym
    end

    def human_name
      @human_name ||= page_collection_name.to_s.humanize
    end
  end

  Spotlight::PagesHelper.module_eval do
    def disable_save_pages_button?
      page_collection_name.to_s == 'about_pages' && @pages.empty?
    end
  end
end
